# Notas

* Nota 1: Projeto em grupo

> grupo: Leandro Alves, Vitor Carrilho, Dilton Costa


[Link do projeto](https://gitlab.com/58303-ghost)

* Observações:

> Documentação: ok

> Organização: Muitos projetos no mesmo grupo

> Funcionalidades: Não foi implementada nenhuma funcionalidade

> Análise de requisitos: Não existe

> diagramas: Nenhum

```
Nota: 7
```

* Nota 2: 


> Q1) 1

> Q2) 0,9 (primeira parte da resposta não cabe, explicou bem a parte do docker)

> Q3) 0,9

> Q4) 0,9

> Q5) 0,5 (faltou relacionar em carro o atributo roda)

> Q6) 1

> Q7) 1 (perfeito)

> Q8) 0,5 (faltou a aplicação)

> Q9) 1 (perfeito)

> Q10) 1

```
Nota: 8,7
```

**Média: 7,85**