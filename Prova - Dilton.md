# Prova de Modelagem de Sistemas

### Senai Dendezeiros
### Curso: Técnico em Desenvolvimento de Sistemas
### Disciplina: Modelagem de Sistemas
### Turma: 58303
### Data: 16/05/2019
### Nome completo: Dilton Costa dos Santos
### Nota:

#### Q1) Documentar essa prova inteira em .md usando MarkDown, utilize o máximo de recursos markdown que puder.

#### Q2) Como utilizar o Docker para containerizar uma aplicação?

> Preparar o ambiente com base nas tecnologias que serão usadas para desenvolver o projeto. No caso de utilizar o PHP, deve ser instalado o Servidor Apache, o PHP e o SGB defindo pela equipe. Usando exemplos do Linux Ubuntu:

``` sudo apt update  ```

``` sudo apt install apache2 ```

```sudo apt instal <SGBD>```

``` sudo apt install php libapache2-mod-php php-<SGBD>```

> Instalar o Docker

> Após realizar a instalação do Docker na máquina, o usuário deve fazer o download da imagem do software/S.O. que será necessário para o projeto. O Download pode ser feito de duas formas: utilizando o comando ```docker pull <nome do software>```, ou usando o comando para inicializar o container ```docker run <comandos específicos>```, esse comando vai buscar a imagem dentro do Docker e se não encontrar vai fazer o download.

> inicializar o container 

``` docker run <comandos específicos>```

> Dentro do projeto, deve-se criar um dockerfile. Nesse arquivo deve-se ter alguns comandos que automatizarão o processo de execução do container:

* FROM - Baixa a imagem desejada
* RUN - Instalar dependências no container
* WORKDIR - Define o diretório do projeto
* CMD - define o comando de inicialização do container



#### Q3) Como o GitLab ajuda na modelagem e documentação de um sistema?

> Através do GitLab podemos disponibilizar a modelagem e a documentação utilizando o formato **.md** para toda a equipe. Dessa maneira a equipe tema acesso a todos os requisitos e à maneira como o sistema funciona, sem depender de um membro específico da equipe, isso facilita a comunicação. Também é possível realizar a modelagem e documentação de forma colaborativa, onde todos os membros podem sugerir alterações e correções nos documentos e no projeto como um todo.

#### Q4) Qual o procedimento completo para fazer o deploy contínuo no GitLab

>Utilizando o exemplo do PHP:

1. Configurar as chaves SSH Públicas e Privadas na máquina que vai acessar o GitLab.

2. Acessar GitLab no navegador e acessar o projeto que será deployado.

3. Entrar na configuração e selecionar a opção CI/CD

4. Na tela de configuração CI/CD inserir as chaves pública e privada e salvar.

5. Criar um arquivo ```.gitlab-ci.yml``` e configurar os estágios do deploy contínuo que podem ser
* Teste
* Build
* Deploy

#### Q5) Sistema onde é possível cadastrar um carro o carro é uma classe e a roda é outra classe. Há uma relação de composição entre a roda e o carro.

![](./carro.png)

#### Q6) Sistema onde é possível cadastrar uma pessoa e um funcionário em uma empresa. Pessoa é uma classe e Funcionário é outra. Funcionário é um tipo de pessoa. Deve haver uma realção de herança entre as classes pessoa e funcionário.

![](./funcionario.png)

#### Q7) Configurar um servidor GCP e associar a um projeto GitLab

1. Acessar GCP;

2. Selecionar as configurações desejadas para criar a instância;

3. Selecionar região onde o servidor ficará;

4. Selecionar S.O. que desejar;

5. Habilitar tráfego HTTP e HTTPS e criar a Instância;

6. Preparar o ambiente de desenvolvimento:
* Baixar Apache - _``` sudo apt install apache2 ```_;
* Baixar Mysql - _``` sudo apt install mysqlserver ```_;
* Baixar PHP - _``` sudo apt install php libapache2-mod-php php-mysql```_;

7. Acessar o diretório do servidor onde devem ficar as aplicações realizar o clone do prjeto no GitLab;

* ```sudo git clone <UrlDoProjetoNoGitlab>```

8. Informar Usuário e senha do GitLab

9. Atrelar o IP do seu servidor a um DNS e fim.

#### Q8) Explique o que é uma branch, um pipiline e um job

> Uma **Branch** é uma cópia de um projeto Git baseado na master de um projeto em GitHub ou GitLab.

> Um **Pipiline** é um modelo de representação dos estágios necessários na criação de uma aplicação.

> Um **Job** é a descrição da forma como os estágio vão ser executados


#### Q9) Qual o procedimento para fazer um CRUD básico e acoplar a programação à um banco de dados? Pode usar como exemplo um CRUD feito em PHP e MYSQL.

1. Criar um formulário onde as informações serão digitadas;

2. Criar banco de dados;

3. Instalar extensão PDO-MYSQL;

4. Criar, de acordo com os padrões MVC, os arquivos que vão realizar a conexão com o banco de dados e tratar as informações recuperadas do formulário para realizar todas as funções do CRUD
* criar arquivo de configuração onde estão os dados de acesso ao banco de dados
* criar arquivo de conexão com métodos que recuperam os dados de acesso e conecta com banco de dados
* criar o arquivo que vai realizar a inserção no banco de dados
* criar o arquivo que vai realizar a consulta desejada no banco de dados
* criar o arquivo que vai deletar as informações desejadas do banco de dados
* criar o arquivo que vai realizar atualizações(update) nas informações do banco de dados


#### Q10) O que foi visto em sala durante o período da disciplina e que mais te motivou? O que vc achou que faltou durante as aulas?

> O que mais me motivou foi aprender sobre Containers, uma tecnologia que há algum tempo eu desejava aprender mas não conseguia por falta de orientação de como começar a estudar

> Para mim as aulas foram muito enriquecedoras e completas.